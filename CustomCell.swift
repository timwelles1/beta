//
//  CustomCell.swift
//  Test
//
//  Created by Eli Alan Harris on 2/2/16.
//  Copyright © 2016 Eli Alan Harris. All rights reserved.
//

import UIKit

import Font_Awesome_Swift


class CustomCell: UITableViewCell {
    
    @IBOutlet var comment: UIButton!
    
    @IBOutlet var time: UILabel!
    
    @IBOutlet var voteDownButton: UIButton!
    
    @IBOutlet var voteUpButton: UIButton!
    
    @IBOutlet internal var cardView: UIView!
    
    @IBOutlet var subject: UILabel!
    
    @IBOutlet var votes: UILabel!
    
    @IBOutlet var realOrFakeBar: UIProgressView!
    
    @IBOutlet var story: UILabel!
    
    @IBOutlet var cardHeaderView: UIView!
    
    var lastVote = 0;
    
    var storyID = 0;
    
    let tales = Tales()
    
    
    
    override func layoutSubviews() {
        
        
        super.layoutSubviews();
        
       
      
        self.voteUpButton.setFAIcon(.FACaretUp, iconSize: 37.0, forState: UIControlState.Normal);
        
        self.voteDownButton.setFAIcon(.FACaretDown, iconSize: 37.0, forState: UIControlState.Normal);
        
        self.comment.setFAIcon(.FACommentO, forState: UIControlState.Normal);

    
        let shadowPath = UIBezierPath(rect: cardView.bounds)
        
        cardView.layer.masksToBounds = false
        cardView.layer.shadowColor = UIColor.blackColor().CGColor
        cardView.layer.shadowOffset = CGSize(width: 0, height: 0.5)
        cardView.layer.shadowOpacity = 0.5;
        cardView.layer.shadowPath = shadowPath.CGPath
        
        
    }
    
    @IBAction func viewComments(sender: AnyObject) {
        
        
        
    }
    
    override func awakeFromNib() {
        
        super.awakeFromNib()
        
        self.selectionStyle = UITableViewCellSelectionStyle.None;
        
        
        
        // Initialization code
    }
    
    
    
    override func setSelected(selected: Bool, animated: Bool) {
        
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    
}
