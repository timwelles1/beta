//
//  ViewController.swift
//  Test
//
//  Created by Eli Alan Harris on 2/2/16.
//  Copyright © 2016 Eli Alan Harris. All rights reserved.
//

import UIKit

import BTNavigationDropdownMenu

import SwiftDate

import DGElasticPullToRefresh

class RecentTaleController: UITableViewController, TalesProtocol {
    
    
    // MARK: IBOutlets
    
    @IBOutlet var postButton: UIBarButtonItem!
    
    
    // MARK: Class Variables
    
    let tales = Tales()
    
    var talesArray:[Tale]?
    
    var promptOfTheDay:Prompt?
    
    let date = NSDate()
    
    
    // MARK: Tale Delegate Methods
    
    func talesDidReturn(tale: [Tale]) {
        
        NSLog("Tales returning");
        
        talesArray = tale;
        
        refreshEnding();
        
        refreshTable();
        
    }
    
    func promptDidReturn(prompt: Prompt) {
        
        
        promptOfTheDay = prompt;
        
        refreshTable();
        
        
    }
    
    
    func postDidReturn() {
        
        
        retrieveTalesAndPrompts();
        
        
    }
    
    
    // MARK: Handle the Segues
    
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        
        if segue.identifier == "post" {
            
            if let postVC = segue.destinationViewController as? UINavigationController {
                
                if let postCont = postVC.viewControllers[0] as? PostController {
                    
                    postCont.talP.delegate = self;
                    
                }
            }
        }
        
        if segue.identifier == "viewStory" {
            
            if let storyView = segue.destinationViewController as? StoryViewController {
                
                if let cell = sender as? CustomCell {
                    
                    storyView.storyText = cell.story.text!
                    
                    storyView.subject = cell.subject.text!;
                    
                }
                
            }
            
        }
        
    }
    
    // MARK: UITableViewFunctions
    
    override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        
        
        let date4 = talesArray![indexPath.row].time.toDate(DateFormat.Custom("yyyy/MM/dd HH:mm:ss"))
        
        let cell = tableView.dequeueReusableCellWithIdentifier("CustomCell") as! CustomCell
        
        cell.storyID = talesArray![indexPath.row].id;
        
        cell.time.text = timeAgoSinceDate(date4!, numericDates: true);
        
        cell.subject.text = talesArray![indexPath.row].subject;
        
        cell.votes.text = String(talesArray![indexPath.row].votes);
        
        cell.story.text = talesArray![indexPath.row].text;
        
        cell.story.numberOfLines = 3;
        
        cell.story.preferredMaxLayoutWidth = 300;
        
        cell.story.lineBreakMode = NSLineBreakMode.ByTruncatingTail;
        
        cell.lastVote = talesArray![indexPath.row].vote;
        
        if(cell.lastVote == 1) {
            
            cell.voteUpButton.tintColor = UIColor(red: 255.0/255.0, green: 149/255.0, blue: 23/255.0, alpha: 1);
            
            cell.voteDownButton.tintColor = UIColor.blackColor()
            
        } else if (cell.lastVote == -1) {
            
            cell.voteDownButton.tintColor = UIColor(red: 255.0/255.0, green: 149/255.0, blue: 23/255.0, alpha: 1);
            
            cell.voteUpButton.tintColor = UIColor.blackColor()
            
            
        } else {
            
            cell.voteDownButton.tintColor = UIColor.blackColor()
            
            cell.voteUpButton.tintColor = UIColor.blackColor()
            
        }
        
        cell.voteUpButton.addTarget(self, action: "voteUp:", forControlEvents: UIControlEvents.TouchUpInside);
        
        cell.voteDownButton.addTarget(self, action: "voteDown:", forControlEvents: UIControlEvents.TouchUpInside);
        
        
        return cell;
        
    }
    
    func voteUp(sender: AnyObject) {
        
        
        var indexPath: NSIndexPath!
        
        if let button = sender as? UIButton {
            
            if let superview = button.superview?.superview?.superview {
                
                if let cell = superview.superview as? CustomCell {
                    
                    indexPath = self.tableView.indexPathForCell(cell);
                    
                    let cell = self.tableView.cellForRowAtIndexPath(indexPath) as! CustomCell
                    
                    if(cell.lastVote != 1) {
                        
                        talesArray![indexPath.row].vote = 1;
                        
                        cell.voteUpButton.tintColor = UIColor(red: 255.0/255.0, green: 149/255.0, blue: 23/255.0, alpha: 1);
                        
                        cell.voteDownButton.tintColor = UIColor.blackColor()
                        
                        
                        if(cell.lastVote == 0) {
                            
                            cell.lastVote = 1;
                            
                            var voteCount = Int(cell.votes.text!)!
                            
                            voteCount = voteCount + 1;
                            
                            talesArray![indexPath.row].votes = voteCount;
                            
                            cell.votes.text = String(voteCount);
                            
                        } else {
                            
                            cell.lastVote = 1;
                            
                            var voteCount = Int(cell.votes.text!)!
                            
                            voteCount = voteCount + 2;
                            
                            talesArray![indexPath.row].votes = voteCount;
                            
                            
                            cell.votes.text = String(voteCount);
                            
                        }
                        
                        
                        tales.upvoteTale(cell.storyID);
                        
                        
                    } else {
                        
                        tales.clearVoteForTale(cell.storyID);
                        
                        cell.voteUpButton.tintColor = UIColor.blackColor();
                        
                        cell.voteDownButton.tintColor = UIColor.blackColor();
                        
                        cell.lastVote = 0;
                        
                        talesArray![indexPath.row].vote = 0;
                        
                        var voteCount = Int(cell.votes.text!)!
                        
                        voteCount = voteCount - 1;
                        
                        talesArray![indexPath.row].votes = voteCount;
                        
                        
                        cell.votes.text = String(voteCount);
                        
                        
                    }
                }
            }
            
        }
        
    }
    
    
    func voteDown(sender: AnyObject) {
        
        var indexPath: NSIndexPath!
        
        if let button = sender as? UIButton {
            
            if let superview = button.superview?.superview?.superview {
                
                if let cell = superview.superview as? CustomCell {
                    
                    indexPath = self.tableView.indexPathForCell(cell);
                    
                    let cell = self.tableView.cellForRowAtIndexPath(indexPath) as! CustomCell
                    
                    
                    if(cell.lastVote != -1) {
                        
                        talesArray![indexPath.row].vote = -1;
                        
                        cell.voteDownButton.tintColor = UIColor(red: 255.0/255.0, green: 149/255.0, blue: 23/255.0, alpha: 1);
                        
                        cell.voteUpButton.tintColor = UIColor.blackColor()
                        
                        if(cell.lastVote == 0) {
                            
                            cell.lastVote = -1;
                            
                            var voteCount = Int(cell.votes.text!)!
                            
                            voteCount = voteCount - 1;
                            
                            talesArray![indexPath.row].votes = voteCount;
                            
                            cell.votes.text = String(voteCount);
                            
                        } else {
                            
                            cell.lastVote = -1;
                            
                            var voteCount = Int(cell.votes.text!)!
                            
                            voteCount = voteCount - 2;
                            
                            talesArray![indexPath.row].votes = voteCount;
                            
                            
                            cell.votes.text = String(voteCount);
                            
                        }
                        
                        tales.downvoteTale(cell.storyID);
                        
                    } else {
                        
                        tales.clearVoteForTale(cell.storyID);
                        
                        cell.voteUpButton.tintColor = UIColor.blackColor();
                        
                        cell.voteDownButton.tintColor = UIColor.blackColor();
                        
                        cell.lastVote = 0;
                        
                        var voteCount = Int(cell.votes.text!)!
                        
                        voteCount = voteCount + 1;
                        
                        talesArray![indexPath.row].votes = voteCount;
                        
                        talesArray![indexPath.row].vote = 0;
                        
                        cell.votes.text = String(voteCount);
                        
                        
                    }
                    
                }
            }
        }
        
    }
    
    
    
    override func tableView(tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        
        
        let cell = tableView.dequeueReusableCellWithIdentifier("HeaderCell") as! HeaderCell
        
        if let promptOfTheDay = promptOfTheDay {
            
            cell.prompt.text? = promptOfTheDay.prompt
            
            
        }
        
        return cell;
        
        
    }
    
    
    override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        if let talesArray = talesArray {
            
            return talesArray.count;
            
            
        } else {
            
            return 0;
        }
        
        
        
    }
    
    override func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        
        return 1;
        
    }
    
    
    
    override func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        
        
        
        
        
        
        
    }
    
    
    
    override func tableView(tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        
        return 52;
        
    }
    
    func refreshTable() {
        
        dispatch_async(dispatch_get_main_queue()) {
            
            self.tableView.reloadData()
        }
        
        
    }
    
    
    func configureTable() {
        
        
        self.tableView.estimatedRowHeight = 200
        
        self.tableView.rowHeight = UITableViewAutomaticDimension
        
    }
    
    
    
    
    
    // MARK: Refresh Methods
    
    func refresh() {
        
        tales.getTales();
        
        tales.getPromptOfTheDay();
        
    }
    
    func refreshEnding() {
        
        
        
    }
    
    func configureRefreshControl() {
        
        let loadingView = DGElasticPullToRefreshLoadingViewCircle()
        
        loadingView.tintColor = self.navigationItem.titleView?.tintColor
        
        tableView.dg_addPullToRefreshWithActionHandler({ [weak self] () -> Void in
            
            self!.refresh();
            
            self?.tableView.dg_stopLoading()
            
            }, loadingView: loadingView)
        
        tableView.dg_setPullToRefreshFillColor(UIColor.darkGrayColor())
        
        tableView.dg_setPullToRefreshBackgroundColor(tableView.backgroundColor!)
        
        //self.refreshControl?.tintColor = UIColor.darkTextColor()
        
        //self.refreshControl?.addTarget(self, action: "refresh:", forControlEvents: UIControlEvents.ValueChanged)
        
    }
    
    // MARK: Various Methods
    
    
    override func viewDidLoad() {
        
        super.viewDidLoad()
        
        let items = ["Most Popular", "Latest", "Trending", "Nearest", "Top Picks"]
        
        let menuView = BTNavigationDropdownMenu(navigationController: self.navigationController, title: items.first!, items: items)
        
        self.navigationItem.titleView = menuView
        
        menuView.didSelectItemAtIndexHandler = {(indexPath: Int) -> () in
            
            print("Did select item at index: \(indexPath)")
            
            
        }
        
        tales.delegate = self;
        
        configureTable();
        
        retrieveTalesAndPrompts();
        
        configureRefreshControl();
        
        
    }
    
    override func didReceiveMemoryWarning() {
        
        super.didReceiveMemoryWarning()
        
        // Dispose of any resources that can be recreated.
    }
    
    
    func retrieveTalesAndPrompts() {
        
        tales.getTales();
        
        tales.getPromptOfTheDay();
        
        
    }
    
    
    
    
}

