//
//  ViewStoryCell.swift
//  Test
//
//  Created by Eli Alan Harris on 2/6/16.
//  Copyright © 2016 Eli Alan Harris. All rights reserved.
//

import UIKit

class ViewStoryCell: UITableViewCell {
    

    @IBOutlet var story: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        self.selectionStyle = UITableViewCellSelectionStyle.None;

        // Initialization code
    }

    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
