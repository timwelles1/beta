//
//  HeaderCell.swift
//  Test
//
//  Created by Eli Alan Harris on 2/2/16.
//  Copyright © 2016 Eli Alan Harris. All rights reserved.
//

import UIKit

class HeaderCell: UITableViewCell {

    @IBOutlet var prompt: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
