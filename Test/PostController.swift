//
//  PostController.swift
//  Test
//
//  Created by Eli Alan Harris on 2/2/16.
//  Copyright © 2016 Eli Alan Harris. All rights reserved.
//

import UIKit

class PostController: UIViewController, UINavigationBarDelegate {
    
    // MARK: Class Variables
    
    var talP = Tales()
    
    
    // MARK: IBActions
    
    @IBAction func postTale(sender: AnyObject) {
        
        
        if(subject.text?.characters.count == 0 || storyTextView.text?.characters.count == 0) {
            
            let alert = UIAlertController(title: "Oh shucks", message: "Fill in a Subject and a Tale!", preferredStyle: UIAlertControllerStyle.Alert)
            alert.addAction(UIAlertAction(title: "Got it!", style: UIAlertActionStyle.Default, handler: nil))
            self.presentViewController(alert, animated: true, completion: nil)
            
            
            
            return;
            
        }
        
        if(subject.text?.characters.count >= 20) {
            
            
            let alert = UIAlertController(title: "Too Many Characters", message: "Please enter 20 characters or less.", preferredStyle: UIAlertControllerStyle.Alert)
            alert.addAction(UIAlertAction(title: "Got it!", style: UIAlertActionStyle.Default, handler: nil))
            self.presentViewController(alert, animated: true, completion: nil)
            
            
            return;
            
            
        }
        
    
        let taleData = Tale(subject: subject.text!, text: storyTextView.text!, time: "0", votes: 0, id: 0, vote:0);
        
        talP.postTale(taleData);
        
        self.dismissViewControllerAnimated(true, completion: {});
        
        
        
    }
    
    @IBAction func cancel(sender: AnyObject) {
        
        self.dismissViewControllerAnimated(true, completion: {});
        
        
    }
    
    // MARK: IBOutlets
    
    @IBOutlet var subjectView: UIView!
    
    @IBOutlet var subject: UITextField!
    
    @IBOutlet var storyTextView: UITextView!
    
    @IBOutlet var navBar: UINavigationBar!
    
    
    // MARK: Various Methods
    
    override func viewDidLoad()
    {
        
        
        super.viewDidLoad()
        
        let color = UIColor.whiteColor()
        
        subject.attributedPlaceholder = NSAttributedString.init(string: "Subject", attributes: [NSForegroundColorAttributeName: color]);
        
        
        let ca = CALayer()
        
        ca.masksToBounds = false;
        
        ca.backgroundColor = UIColor.orangeColor().CGColor
        
        ca.frame = CGRect.init(x: 0, y: self.subjectView.frame.height, width: super.view.frame.width, height: 3.0);
        
        ca.shadowRadius =  2;
        
        ca.shadowOpacity = 0.3;
        
        
        self.subjectView.layer.addSublayer(ca);
        
        
        // Do any additional setup after loading the view.
    }
    
    override func didReceiveMemoryWarning() {
        
        super.didReceiveMemoryWarning()
        
        // Dispose of any resources that can be recreated.
    }
    
    
}
