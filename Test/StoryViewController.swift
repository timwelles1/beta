//
//  StoryViewController.swift
//  Test
//
//  Created by Eli Alan Harris on 2/6/16.
//  Copyright © 2016 Eli Alan Harris. All rights reserved.
//

import UIKit

class StoryViewController: UITableViewController {
    
    
    var storyText: String!
    
    var subject: String!
    
    
    
    override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return 1;
        
    }
    
    
    override func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        
        
        return 2;
        
    }
    
    
    override func tableView(tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        
        if(section == 0) {
            
            return self.subject;
            
        } else {
            
            return "Comments"
            
        }
        
    }
    
    override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        
        let cell = UITableViewCell()
        
        if(indexPath.section == 0) {
            
            if let story = tableView.dequeueReusableCellWithIdentifier("StoryCell") as? ViewStoryCell {
                
                story.story?.numberOfLines = 0;
                
                story.story?.text = self.storyText;
                
                return story;
                
                
            }
            
        }
        
        if(indexPath.section == 1) {
            
            if let commment = tableView.dequeueReusableCellWithIdentifier("Comment") as? CommentCell {
                
                commment.comment?.numberOfLines = 100;
                
                commment.comment?.text = "So funny";
                
                return commment;
                
                
            }
            
            
        }
        
        
        return cell;
        
    }
    
    override func viewDidLoad() {
        
        super.viewDidLoad();
        
        tableView.reloadData()
        
        self.tableView.estimatedRowHeight = 500
        
        self.tableView.rowHeight = UITableViewAutomaticDimension;
        
        
    }
    
    
}
