//
//  Tales.swift
//  Test
//
//  Created by Eli Alan Harris on 2/2/16.
//  Copyright © 2016 Eli Alan Harris. All rights reserved.
//

import UIKit

import Alamofire

import SwiftyJSON

// MARK: TalesProtocol Delegate Callbacks


protocol TalesProtocol {
    
    func talesDidReturn(tale:[Tale]);
    
    func postDidReturn();
    
    func promptDidReturn(prompt: Prompt);
    
}

// MARK: Tale Post Structure

struct Tale {
    
    
    var subject:String
    
    var text:String
    
    var time:String
    
    var votes:NSInteger
    
    var id:NSInteger
    
    var vote:NSInteger
    
    
    
    
}

// MARK: Prompt Structure

struct Prompt {
    
    
    var prompt:String!
    
}


class Tales: NSObject {
    
    // MARK: Class Variables
    
    var delegate: TalesProtocol?
    
    private let identifier = UIDevice.currentDevice().identifierForVendor!.UUIDString;
    
    
    private let newPostURL: NSURL = NSURL(string: "http://ec2-52-90-189-108.compute-1.amazonaws.com")!
    
    private let getTalesURL: NSURL = NSURL(string: "http://ec2-52-90-189-108.compute-1.amazonaws.com")!
    
    private let getPromptURL: NSURL = NSURL(string: "http://ec2-52-90-189-108.compute-1.amazonaws.com/prompts")!
    
    private let votingURL: NSURL = NSURL(string: "http://ec2-52-90-189-108.compute-1.amazonaws.com/vote")!
    
    private let session = NSURLSession.sharedSession()
    
    
    
    //MARK: Retrieving and Posting Tales
    
    func postTale(data:Tale) {
        
        
        let request = NSMutableURLRequest(URL:newPostURL);
        
        let bodyData = "subject=" + "\(data.subject)" + "&text=" + "\(data.text)"
        
        request.HTTPMethod = "POST"
        
        request.HTTPBody = bodyData.dataUsingEncoding(NSUTF8StringEncoding);
        
        session.dataTaskWithRequest(request, completionHandler: { (data, reresponsesp , error) in
            
            //Verify an error has not occured
            
            if(!(error == nil)) {
                
                NSLog("There was an error posting the Tale");
                
            }
            
            self.delegate?.postDidReturn();
            
            
        }).resume();
        
        
        
    }
    
    func getPromptOfTheDay() {
        
        
        //Add the task to the session and start it
        
        session.dataTaskWithURL(getPromptURL, completionHandler: { (data, reresponsesp , error) in
            
            if let returnedJSON = data {
                
                do {
                    
                    /* Now we read the json object and convert it to an array */
                    
                    let jsonData = try NSJSONSerialization.JSONObjectWithData(returnedJSON, options: NSJSONReadingOptions.MutableContainers) as! NSArray
                    
                    //Verify that there is a prompt of the day
                    
                    if(jsonData.count > 0) {
                        
                        //We know there is only one prompt so index the first one and return it
                        
                        self.delegate?.promptDidReturn(Prompt(prompt: jsonData[0].valueForKey("prompt") as! String))
                        
                    }
                    
                } catch {
                    
                    NSLog("There was an error parsing the prompt data");
                    
                    
                }
                
                
            }
            
            
            /* Start the instantiated task */
            
        }).resume();
        
        
        
    }
    
    func getTales() {
        
//        Alamofire.request(.GET, getTalesURL, parameters: ["identifier":identifier]).responseJSON { response in
//           
//            let json = JSON(response.result.value!);
//            
//            for(_,subJSON):(String, JSON) in json {
//                
//                print(subJSON["vote"]);
//            }
//            
//        }
        
        let getTalesString = "http://ec2-52-90-189-108.compute-1.amazonaws.com/?identifier=" + identifier;
        
        let talURL: NSURL = NSURL(string: getTalesString)!
        
        
        var talesArray = [Tale]()
        
        
        /* Now we set the http post data */
        
        
        
        session.dataTaskWithURL(talURL, completionHandler: { (data, response, error) in
            
            if error != nil {
                
                print(error)
                
            } else {
                
                if let data = data {
                    
                    do {
                        
                        let jsonResult = try NSJSONSerialization.JSONObjectWithData(data, options: NSJSONReadingOptions.MutableContainers) as! NSArray
                        
                        for tale in jsonResult {
                            
                            if let v = tale["vote"]! as? NSInteger {
                                
                                
                                
                                talesArray.append(Tale(subject: tale["subject"] as! String, text: tale["text"] as! String, time: tale["created_at"] as! String, votes: tale["votes"] as! NSInteger, id: tale["id"] as! NSInteger,  vote: v))
                                
                                
                                
                            } else {
                                
                                talesArray.append(Tale(subject: tale["subject"] as! String, text: tale["text"] as! String, time: tale["created_at"] as! String, votes: tale["votes"] as! NSInteger, id: tale["id"] as! NSInteger, vote: 0))
                                
                            }
                            
                            
                        }
                    } catch {}
                    
                    self.delegate?.talesDidReturn(talesArray);
                    
                }
            }
            
        }).resume();
        
        
    }
    
    // MARK: Up and Down Voting
    
    func upvoteTale(taleID: NSInteger) {
        
        
        /* Create the request */
        
        let request = NSMutableURLRequest(URL: votingURL);
        
        /* Set Our Vote Data */
        
        let voteData = "story_id=\(taleID)&vote=1&identifier=\(identifier)";
        
        /* Set our post method */
        
        request.HTTPMethod = "POST";
        
        /* Now we set the http post data */
        
        request.HTTPBody = voteData.dataUsingEncoding(NSStringEncoding(NSUTF8StringEncoding));
        
        /* Create the session with the request */
        
        session.dataTaskWithRequest(request, completionHandler: {(data, resp, error) -> Void in
            
            
        }).resume();
        
        
        
        
    }
    
    
    func downvoteTale(taleID: NSInteger) {
        
        /* Create the request */
        
        let request = NSMutableURLRequest(URL: votingURL);
        
        /* Set Our Vote Data */
        
        let voteData = "story_id=\(taleID)&vote=-1&identifier=\(identifier)";
        
        /* Set our post method */
        
        request.HTTPMethod = "POST";
        
        /* Now we set the http post data */
        
        request.HTTPBody = voteData.dataUsingEncoding(NSStringEncoding(NSUTF8StringEncoding));
        
        /* Create the session with the request */
        
        session.dataTaskWithRequest(request, completionHandler: {(data, resp, error) -> Void in
            
            
        }).resume();
        
        
    }
    
    
    func clearVoteForTale(taleID: NSInteger) {
        
        /* Create the request */
        
        let request = NSMutableURLRequest(URL: votingURL);
        
        /* Set Our Vote Data */
        
        let voteData = "story_id=\(taleID)&vote=0&identifier=\(identifier)";
        
        /* Set our post method */
        
        request.HTTPMethod = "POST";
        
        /* Now we set the http post data */
        
        request.HTTPBody = voteData.dataUsingEncoding(NSStringEncoding(NSUTF8StringEncoding));
        
        /* Create the session with the request */
        
        session.dataTaskWithRequest(request, completionHandler: {(data, resp, error) -> Void in
            
            
        }).resume();
        
        
    }
    
    // MARK: Retrieving Comments
    
    func getCommentsForTale(taleID: NSInteger) {
        
        
        
    }
}
